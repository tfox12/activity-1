<?php


require_once 'SuperHero.php';

$ultraMan = new SuperHero("Batman");
$unstoppable = new SuperHero("Superman");

while ($ultraMan->isHeDead() == "alive" && $unstoppable->isHeDead() == "alive") {
    $ultraMan->attack($unstoppable);
    echo $unstoppable->getName() . " has " . $unstoppable->getHealth() . "health left.<br>";
    
    $unstoppable->attack ($ultraMan);
    echo $ultraMan->getName() . " has " . $ultraMan->getHealth() . "health left.<br>";
    echo "<hr>";
    
}