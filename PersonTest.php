<?php
require_once 'Person.php';

$person1 = new Person("Mark");

$person2 = new Person("Samuel");

$person1->walk();
$person2->walk();

$person1->greeting();

$person1->formalGreeting();

$person1->spanishGreeting();

$person2->login("Norman", "password1");

$person1->login("Bob", "Tyler");