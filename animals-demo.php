<?php

require_once 'Dog.php';
require_once 'Cat.php';

$cat1 = new Cat("Lame", "Black");
$dog1 = new Dog("Cool", "Yellow");

$cat1->talk();
$dog1->talk();

$cat1->dotrick();
$dog1->dotrick();