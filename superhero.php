<?php

require_once 'Person.php';

class superHero extends Person {
    
    private $hasCape;
    private $health;
    private $isDead;
    
    /**
     * @return number
     */
    public function getHealth()
    {
        return $this->health;
    }

    /**
     * @param number $health
     */
    public function setHealth($health)
    {
        $this->health = $health;
    }
    
    public function takeDamage($hitAgainstMe) {
        $this->health = $this->health - $hitAgainstMe;
        if ($this->health <=0) {
            $this->isDead = ;
        }
    }
    
    public function attack($enemy) {
        // Pick a random number from 1-10
        // Subtract number from enemy health
        $damage = rand(1,10);
        $enemy->takeDamage($damage);
        echo $this->name . " has attacked " . $enemy->name . " and caused " . $damage . "damage. <br>";
    }
    
    public function __construct($n) {
        $this->name = $n;
        $this->health = 100;
        $this->isDead = false;
    }
    
    public function isHeDead() {
        if ($this->isDead) {
            return "dead";
        }
        else {
            return "alive";
        }
    }
    
    public function obtainACape() {
        $this->hasCape = true;
    }
    
    public function loseCape() {
        $this->hasCape = false;
    }
    
    public function capeStatus() {
        if ($this->hasCape == true) {
            echo $this->name . " has a cape.<br>";
        }
        else {
            echo $this->name . " does not have a cape.<br>";
        }
    }
}

?>