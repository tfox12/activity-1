<?php
class car {
    
    private $tires;
    private $hasEngine;
    private $tirePressure;
    private $isRunning;
    
    public function addTires($numberOfNewTires) {
        if ($numberOfNewTires > 0 && $numberOfNewTires <=4) {
            if ($this->tires + $numberOfNewTires > 4) {
                echo "You can only have 4 tires. Try again.<br>";
            }
            else {
                $this->tires = $this->tires + $numberOfNewTires;
                echo "You installed " . $numberOfNewTires . " you now have " . $this->tires . " tires on your car. <br>";
            }
        }
        else {
            echo "Wrong amount of tires.<br>";
        }
    }
    
    public function addEngine($numberOfEngines) {
        if ($numberOfEngines > 0 && $numberOfEngines <2) {
            if($this->hasEngine + $numberOfEngines > 1) {
                echo "you can only have one engine. Try again. <br>";
            }
            else {
                $this->hasEngine = $this->hasEngine + $numberOfEngines;
                echo "You installed " . $numberOfEngines . " You now have " . $this->hasEngine . " Engines in your car. <br>";
            }
        }   
    }
    
    public function addPressure($tirePsi) {
        if ($tirePsi >= 32) {
            if($this->tirePressure + $tirePsi >= 32) {
            echo "tire pressure is passed 32 and functional. <br>";    
            }
        }
    }
    
    public function startEngine($isRunning) {
        if ($isRunning == "on") {
            echo "Engine is running. <br>";
        }
        if ($isRunning == "off") {
            echo "Engine is off. <br>";
        }
    }
    
    public function addSpeed($milesPerHour) {
        if ($milesPerHour >= 1 && $milesPerHour <=60) {
            echo "You are traveling at " . $milesPerHour . " miles per hour. <br>";
        }
        else {
            echo "You are stopped. <br>";
        }
    }
}

?>