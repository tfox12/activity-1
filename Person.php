<?php

class Person {
    
    private $name;
    private $age;
    private $username;
    private $password;
    
    public function __construct($n) {
        $this->name = $n;
        echo "Hello I am a new person. My name is " . $n . "<br>";
        $this->password = "Tyler";
        $this->username = "Bob";
    
    }
    
    public function walk() {
        echo $this->name . " is walking...<br>";
    }
    
    public function greeting() {
        echo "hello.<br>";
    }
    
    public function formalGreeting() {
        echo "why hello there. <br>";
    }
    
    public function spanishGreeting() {
        echo "Hola! Como Estas? <br>";
    }
    
    public function login($a, $b) {
        if ($a == $this->username && $b == $this->password) {
            echo "logged in<br>";
        }
        else {
            echo "Something is wrong. Login fail.<br>";
        }

    }
    
}


?>